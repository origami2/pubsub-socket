var assert = require('assert');
var ConnectedEmitters = require('origami-connected-emitters');
var PubSubSocket = require('..');

describe('PubSub socket', function () {
  it('requires a socket', function () {
    assert.throws(
      function () {
        new PubSubSocket();
      },
      /socket is required/
    );
  });
  
  it('returns an object with .emit,.on,.off functions', function () {
    var pubsub = new PubSubSocket((new ConnectedEmitters()).createEmitter());
    
    assert(pubsub);
    assert('function', typeof(pubsub.emit));
    assert('function', typeof(pubsub.on));
    assert('function', typeof(pubsub.off));
  });
  
  it('emits subscribe on the other end', function (done) {
    var ce = new ConnectedEmitters();
    
    var otherEnd = ce.createEmitter();
    
    var pubsub = new PubSubSocket(ce.createEmitter());
    
    otherEnd
    .on(
      'subscribe', 
      function (namespace) {
        try {
          assert.equal(namespace, 'my-event');
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    pubsub
    .on(
      'my-event',
      function () {
        
      }
    );
  });
    
  it('emits event on this end', function (done) {
    var ce = new ConnectedEmitters();
    
    var otherEnd = ce.createEmitter();
    
    var pubsub = new PubSubSocket(ce.createEmitter());
    
    pubsub
    .on(
      'my-event',
      function (message, callback) {
        try {
          assert.deepEqual(
            { something: 'yes' },
            message
          );
          assert.equal('function', typeof(callback));
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    otherEnd.emit('event', 'my-event', { something: 'yes' }, function () {});
  });
    
  it('emits event on the other end', function (done) {
    var ce = new ConnectedEmitters();
    
    var otherEnd = ce.createEmitter();
    
    var pubsub = new PubSubSocket(ce.createEmitter());
    
    otherEnd
    .on(
      'publish',
      function (eventName, message, callback) {
        try {
          assert.equal('my-event', eventName);
          assert.deepEqual({ something: 'yes' }, message);
          assert.equal('function', typeof(callback));
          
          done();
        } catch (e) {
          done(e);
        }
      }
    );
    
    pubsub
    .emit(
      'my-event',
      { something: 'yes' },
      function () {
      }
    );
  });
    
  it('do not emits if handler is removed through .off', function (done) {
    var ce = new ConnectedEmitters();
    
    var otherEnd = ce.createEmitter();
    
    var pubsub = new PubSubSocket(ce.createEmitter());
    
    var handler = function () {
      done(new Error('this shouldn\'t have happended'));
    };
    
    pubsub
    .on(
      'my-event',
      handler
    );
    
    pubsub
    .off(
      'my-event',
      handler
    );
    
    otherEnd.emit('event', 'my-event');
    
    done();
  });
});